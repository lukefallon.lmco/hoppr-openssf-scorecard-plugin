from typing import Any, AnyStr, Dict, Optional

from _typeshed import Incomplete

basestring: Incomplete

class PackageURL:
    name: str
    namespace: Optional[str]
    qualifiers: Dict[str, str]
    subpath: Optional[str]
    type: str
    version: str
    def __new__(
        self,
        type: Optional[AnyStr] = ...,
        namespace: Optional[AnyStr] = ...,
        name: Optional[AnyStr] = ...,
        version: Optional[AnyStr] = ...,
        qualifiers: Dict[str, str] = ...,
        subpath: Optional[AnyStr] = ...,
    ) -> PackageURL: ...
    def __hash__(self) -> int: ...
    def to_dict(self, encode: Optional[bool] = ..., empty: Any = ...) -> Dict[str, Any]: ...
    def to_string(self) -> str: ...
    @classmethod
    def from_string(cls, purl: Optional[str]) -> PackageURL: ...
