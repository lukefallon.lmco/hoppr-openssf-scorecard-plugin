"""
Test module for the hoppr_openssf_scorecard.models.libraries module
"""

from __future__ import annotations

from typing import Any

from hoppr_openssf_scorecard.models.libraries import LibrariesIOResponse


def test__getitem__(libraries_io_response: list[dict[str, Any]]):
    """
    Test LibrariesIOResponse.__getitem__ method
    """
    response = LibrariesIOResponse.parse_obj(libraries_io_response)

    assert response[0].repository_url == "https://github.com/hoppr/test-pypi-pkg"
    assert response[1].repository_url == "https://github.com/hoppr/another-test"
