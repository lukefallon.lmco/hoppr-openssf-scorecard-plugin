"""
Test module for the HopprScorecardPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import logging
import re
import test

from copy import deepcopy
from datetime import datetime
from pathlib import Path
from typing import Any, TypeVar

import httpx
import pytest

from hoppr import Component, ExternalReference, Manifest, Property
from packaging.version import InvalidVersion
from packaging.version import parse as parse_version
from pytest import FixtureRequest, LogCaptureFixture, MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers import (
    DebScorecardHelper,
    GitHubScorecardHelper,
    GitScorecardHelper,
    GolangScorecardHelper,
    HelmScorecardHelper,
    LibrariesIOScorecardHelper,
    MavenScorecardHelper,
    NPMScorecardHelper,
    PyPIScorecardHelper,
    RPMScorecardHelper,
    RubyGemsScorecardHelper,
)
from hoppr_openssf_scorecard.models.scorecard import ScorecardResponse
from hoppr_openssf_scorecard.plugin import HopprScorecardPlugin

ExpectedException = TypeVar("ExpectedException", bound=BaseException)


@pytest.fixture(name="plugin", scope="function", params=[dict(plugin_class=HopprScorecardPlugin)])
def plugin_fixture(plugin: HopprScorecardPlugin, config: dict[str, str]) -> HopprScorecardPlugin:
    """
    Override and parametrize plugin_fixture to return HopprScorecardPlugin
    """
    return plugin


@pytest.fixture(name="scorecard_response", scope="function")
def scorecard_response_fixture(request: FixtureRequest) -> dict[str, Any]:
    """
    Fixture to simulate response from OpenSSF Scorecard API
    """
    return {
        "date": datetime.now().strftime("%Y-%m-%d"),
        "metadata": [],
        "repo": {
            "name": getattr(request, "param", "github.com/hoppr/test-project"),
            "commit": "abcdef0123456789abcdef0123456789abcdef01",
        },
        "scorecard": {"version": "v4.10.2", "commit": "376f465c111c39c6a5ad7408e8896cd790cb5219"},
        "score": 7.8,
        "checks": [
            {"name": "Binary-Artifacts", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Branch-Protection", "score": -1, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "CI-Tests", "score": -1, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "CII-Best-Practices", "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Code-Review", "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Contributors", "score": 6, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Dangerous-Workflow", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Dependency-Update-Tool", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Fuzzing", "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "License", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Maintained", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Packaging", "score": -1, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Pinned-Dependencies", "score": 9, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "SAST", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Security-Policy", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Signed-Releases", "score": -1, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Token-Permissions", "score": 10, "reason": "", "documentation": {"short": "", "url": ""}},
            {"name": "Vulnerabilities", "score": -1, "reason": "", "documentation": {"short": "", "url": ""}},
        ],
    }


def test__add_scorecard_properties(plugin: HopprScorecardPlugin, scorecard_response: dict[str, Any]):
    """
    Test HopprScorecardPlugin._add_scorecard_properties method
    """
    component = plugin.context.delivered_sbom.components[0]

    # Test that component is skipped if not found in scorecard map
    result = plugin._add_scorecard_properties(component)
    assert result.is_skip()
    assert result.message == f"No scorecard data available for component: '{component}'"

    # Add component to scorecard map and re-run
    plugin.scorecard_map[component] = ScorecardResponse(**scorecard_response)
    result = plugin._add_scorecard_properties(component)

    assert isinstance(result.return_obj, Component)
    assert result.return_obj.properties is not None

    expected_properties = {
        "hoppr:scorecard:date": datetime.now().strftime("%Y-%m-%d"),
        "hoppr:scorecard:repo:name": "github.com/hoppr/test-project",
        "hoppr:scorecard:repo:commit": "abcdef0123456789abcdef0123456789abcdef01",
        "hoppr:scorecard:scorecard:version": "v4.10.2",
        "hoppr:scorecard:scorecard:commit": "376f465c111c39c6a5ad7408e8896cd790cb5219",
        "hoppr:scorecard:score": "7.8",
        "hoppr:scorecard:check:Binary-Artifacts": "10",
        "hoppr:scorecard:check:Branch-Protection": "-1",
        "hoppr:scorecard:check:CI-Tests": "-1",
        "hoppr:scorecard:check:Contributors": "6",
        "hoppr:scorecard:check:Dangerous-Workflow": "10",
        "hoppr:scorecard:check:Dependency-Update-Tool": "10",
        "hoppr:scorecard:check:License": "10",
        "hoppr:scorecard:check:Maintained": "10",
        "hoppr:scorecard:check:Packaging": "-1",
        "hoppr:scorecard:check:Pinned-Dependencies": "9",
        "hoppr:scorecard:check:SAST": "10",
        "hoppr:scorecard:check:Security-Policy": "10",
        "hoppr:scorecard:check:Signed-Releases": "-1",
        "hoppr:scorecard:check:Token-Permissions": "10",
        "hoppr:scorecard:check:Vulnerabilities": "-1",
    }

    for name, value in expected_properties.items():
        assert Property(name=name, value=value) in result.return_obj.properties


def test__add_vcs_url_external_reference(plugin: HopprScorecardPlugin):
    """
    Test HopprScorecardPlugin._add_vcs_url_external_reference method
    """
    Manifest.load(Path(test.__file__).parent / "resources" / "manifest.yml")

    component = plugin.context.delivered_sbom.components[0]
    assert component is not None

    plugin._add_vcs_url_external_reference(component, repo_url="https://github.com/hoppr/test")

    external_reference = ExternalReference(
        url="https://github.com/hoppr/test",
        type="vcs",  # pyright: ignore[reportGeneralTypeIssues]
        comment=None,
        hashes=None,
    )

    assert plugin.context.delivered_sbom.components[0].externalReferences is not None
    assert external_reference in plugin.context.delivered_sbom.components[0].externalReferences


@pytest.mark.parametrize(argnames="repo_url", argvalues=[["https://github.com/hoppr/test-project"], []])
async def test__create_vcs_url_map(plugin: HopprScorecardPlugin, repo_url: list[str], monkeypatch: MonkeyPatch):
    """
    Test HopprScorecardPlugin._create_vcs_url_map method
    """

    async def _mock_get_vcs_repo_url(cls, purl_string: str) -> list[str]:
        return repo_url

    # Populate list of components to return from `_component_generator` method
    components = []
    for purl_type in ["deb", "gem", "git", "github", "golang", "helm", "maven", "npm", "pypi", "rpm"]:
        component = Component(
            name=f"hoppr-test-{purl_type}",
            purl=f"pkg:{purl_type}/hoppr-test-{purl_type}@1.2.3",
            version="1.2.3",
            type="file",
        )  # pyright: ignore

        # Add duplicates to test already processed
        components.extend([component] * 2)

    monkeypatch.setattr(target=plugin.context.delivered_sbom, name="components", value=components)

    # Patch methods being tested elsewhere
    for helper in [
        DebScorecardHelper,
        GitScorecardHelper,
        GitHubScorecardHelper,
        GolangScorecardHelper,
        HelmScorecardHelper,
        LibrariesIOScorecardHelper,
        MavenScorecardHelper,
        NPMScorecardHelper,
        PyPIScorecardHelper,
        RPMScorecardHelper,
        RubyGemsScorecardHelper,
    ]:
        monkeypatch.setattr(target=helper, name="get_vcs_repo_url", value=_mock_get_vcs_repo_url)

    await plugin._create_vcs_url_map()


async def test__create_vcs_url_map_missing_purl(
    plugin: HopprScorecardPlugin, monkeypatch: MonkeyPatch, caplog: LogCaptureFixture
):
    """
    Test HopprScorecardPlugin._create_vcs_url_map method with component missing `purl` property
    """

    # Populate list of components with a purl of None to return from `_component_generator` method
    components = [
        Component.parse_obj({"name": "hoppr-test-none", "purl": None, "version": "1.2.3", "type": "file"}),
        Component.parse_obj(
            {"name": "hoppr-test-none", "bom-ref": "hoppr-test-none@1.2.3", "purl": None, "type": "file"}
        ),
    ]

    monkeypatch.setattr(target=plugin.context.delivered_sbom, name="components", value=components)

    await plugin._create_vcs_url_map()

    assert caplog.records[0].message == "Component hoppr-test-none@1.2.3 is missing the 'purl' property."
    assert caplog.records[1].message == "Component hoppr-test-none is missing the 'purl' property."


async def test__create_vcs_url_map_unsupported_purl_type(
    plugin: HopprScorecardPlugin, monkeypatch: MonkeyPatch, caplog: LogCaptureFixture
):
    """
    Test HopprScorecardPlugin._create_vcs_url_map method with unsupported purl type
    """

    async def _mock_get_vcs_repo_url(cls, purl_string: str) -> list[str]:
        return []

    # Populate list of components with an unsupported purl type to return from `_component_generator` method
    components = [
        Component(
            name="hoppr-test-unsupported",
            purl="pkg:unsupported/hoppr-test-unsupported@1.2.3",
            version="1.2.3",
            type="file",
        ),  # pyright: ignore
    ]

    monkeypatch.setattr(target=plugin.context.delivered_sbom, name="components", value=components)
    monkeypatch.setattr(target=LibrariesIOScorecardHelper, name="get_vcs_repo_url", value=_mock_get_vcs_repo_url)
    monkeypatch.setattr(target=GitHubScorecardHelper, name="get_vcs_repo_url", value=_mock_get_vcs_repo_url)

    with caplog.at_level(level=logging.INFO):  # , logger="HopprPytest"):
        await plugin._create_vcs_url_map()

    assert caplog.records[2].message == "No helper exists for PURL type: 'unsupported'"


@pytest.mark.parametrize(
    argnames=["external_references", "expected_result"],
    argvalues=[
        (
            [
                ExternalReference(
                    url="https://github.com/hoppr/test-project",
                    type="vcs",  # pyright: ignore
                    comment=None,
                    hashes=None,
                )
            ],
            ["https://github.com/hoppr/test-project"],
        ),
        (
            [
                ExternalReference(
                    url="https://github.com/hoppr/test-project",
                    type="vcs",  # pyright: ignore
                    comment=None,
                    hashes=None,
                ),
                ExternalReference(
                    url="https://github.com/hoppr/test-project/releases",
                    type="distribution",  # pyright: ignore
                    comment=None,
                    hashes=None,
                ),
            ],
            ["https://github.com/hoppr/test-project", "https://github.com/hoppr/test-project/releases"],
        ),
        ([], []),
    ],
)
async def test__get_repo_from_component(
    plugin: HopprScorecardPlugin, external_references: list[ExternalReference], expected_result: list[str]
):
    """
    Test HopprScorecardPlugin._get_repo_from_component method
    """
    component = Component(
        name="TestComponent",
        purl="pkg:pypi/hoppr/hippo@1.2.3",
        type="file",
        externalReferences=external_references,
    )  # pyright: ignore

    repo_url = plugin._get_repo_from_component(component)
    assert repo_url == expected_result


@pytest.mark.parametrize(
    argnames=["response_status_code", "repo_owner", "repo_name", "scorecard_response"],
    argvalues=[
        (200, "hoppr", "test-project", "github.com/hoppr/test-project"),
        (200, "hoppr", "other-project", "github.com/hoppr/other-project"),
        (200, "hoppr", "yet-another-project", "github.com/hoppr/yet-another-project"),
    ],
    indirect=["scorecard_response"],
)
async def test__query_scorecard_api(  # pylint: disable=too-many-arguments
    plugin: HopprScorecardPlugin,
    response_status_code: int,
    scorecard_response: dict[str, Any],
    repo_owner: str,
    repo_name: str,
    httpx_mock: HTTPXMock,
):
    """
    Test HopprScorecardPlugin._query_scorecard_api method
    """
    httpx_mock.add_response(
        url=re.compile("^https://api.securityscorecards.dev/projects/.*$"),
        status_code=response_status_code,
        json=scorecard_response,
    )

    result = await plugin._query_scorecard_api(repo_owner, repo_name)
    assert result == ScorecardResponse(**scorecard_response)


async def test__query_scorecard_api_url_not_found_error(
    plugin: HopprScorecardPlugin,
    caplog: LogCaptureFixture,
    httpx_mock: HTTPXMock,
    monkeypatch: MonkeyPatch,
):
    """
    Test HopprScorecardPlugin._query_scorecard_api method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile("^https://api.securityscorecards.dev/projects/.*$"), status_code=404)

    monkeypatch.setattr(target=plugin, name="_logger", value=logging.getLogger("HopprPytest"))
    with caplog.at_level(level=logging.INFO, logger="HopprPytest"):
        result = await plugin._query_scorecard_api(repo_owner="hoppr", repo_name="test-project")
        assert result is None

    assert caplog.messages[0] == "Requesting Scorecard data for repository 'github.com/hoppr/test-project'"
    assert caplog.messages[1] == "Scorecard data not found for repository 'github.com/hoppr/test-project'"


async def test__query_scorecard_api_timeout(plugin: HopprScorecardPlugin, httpx_mock: HTTPXMock):
    """
    Test HopprScorecardPlugin._query_scorecard_api method returns None if timeout occurs
    """
    httpx_mock.add_exception(
        url=re.compile("^https://api.securityscorecards.dev/projects/.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await plugin._query_scorecard_api(repo_owner="hoppr", repo_name="test-project")
        assert result is None


def test_get_version(plugin: HopprScorecardPlugin):
    """
    Test HopprScorecardPlugin.get_version method
    """
    version = plugin.get_version()

    try:
        parse_version(version)
    except InvalidVersion:
        pytest.fail(f"Invalid version string: '{version}'")


def test_pre_stage_process(plugin: HopprScorecardPlugin, monkeypatch: MonkeyPatch):
    """
    Test HopprScorecardPlugin.pre_stage_process method
    """

    async def _mock_get_vcs_repo_url(cls, purl_string: str) -> list[str]:
        return ["https://github.com/hoppr/test-project"]

    # monkeypatch.setattr(target=plugin, name="_create_vcs_url_map", value=_mock_create_vcs_url_map)
    monkeypatch.setattr(target=GolangScorecardHelper, name="get_vcs_repo_url", value=_mock_get_vcs_repo_url)

    external_ref = ExternalReference(
        url="https://github.com/hoppr/test-project",
        comment=None,
        hashes=None,
        type="vcs",  # pyright: ignore
    )

    expected_sbom = deepcopy(plugin.context.delivered_sbom)
    expected_sbom.components[0].externalReferences = []
    expected_sbom.components[0].externalReferences.append(external_ref)  # pyright: ignore

    result = plugin.pre_stage_process()

    assert result.is_success()
    assert result.return_obj == expected_sbom


def test_process_component(plugin: HopprScorecardPlugin, scorecard_response: dict[str, Any], monkeypatch: MonkeyPatch):
    """
    Test HopprScorecardPlugin.process_component method
    """

    def _mock_query_scorecard_api(return_value: ScorecardResponse | None):
        async def _query_scorecard_api(platform: str, repo_owner: str, repo_name: str) -> ScorecardResponse | None:
            return return_value

        return _query_scorecard_api

    component = plugin.context.delivered_sbom.components[0]
    component.externalReferences = []

    result = plugin.process_component(component)
    assert result.is_skip()
    assert result.message == "No VCS repository metadata associated with component."

    external_ref = ExternalReference(
        url="https://github.com/hoppr/test-project",
        comment=None,
        hashes=None,
        type="vcs",  # pyright: ignore
    )

    component.externalReferences.append(external_ref)

    expected_component = deepcopy(component)
    expected_component.properties = [
        Property.parse_obj({"name": "hoppr:scorecard:date", "value": datetime.now().strftime("%Y-%m-%d")}),
        Property.parse_obj({"name": "hoppr:scorecard:repo:name", "value": "github.com/hoppr/test-project"}),
        Property.parse_obj(
            {"name": "hoppr:scorecard:repo:commit", "value": "abcdef0123456789abcdef0123456789abcdef01"}
        ),
        Property.parse_obj({"name": "hoppr:scorecard:scorecard:version", "value": "v4.10.2"}),
        Property.parse_obj(
            {"name": "hoppr:scorecard:scorecard:commit", "value": "376f465c111c39c6a5ad7408e8896cd790cb5219"}
        ),
        Property.parse_obj({"name": "hoppr:scorecard:score", "value": "7.8"}),
        Property.parse_obj({"name": "hoppr:scorecard:metadata", "value": ""}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Binary-Artifacts", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Branch-Protection", "value": "-1"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:CI-Tests", "value": "-1"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Contributors", "value": "6"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Dangerous-Workflow", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Dependency-Update-Tool", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:License", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Maintained", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Packaging", "value": "-1"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Pinned-Dependencies", "value": "9"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:SAST", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Security-Policy", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Signed-Releases", "value": "-1"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Token-Permissions", "value": "10"}),
        Property.parse_obj({"name": "hoppr:scorecard:check:Vulnerabilities", "value": "-1"}),
    ]

    monkeypatch.setattr(target=plugin, name="_query_scorecard_api", value=_mock_query_scorecard_api(return_value=None))

    result = plugin.process_component(component)
    assert result.is_skip()
    assert result.message == (
        "Scorecard data not found for component: 'pkg:golang/hoppr-test.com/hoppr/test-project@1.2.3'"
    )

    monkeypatch.setattr(
        target=plugin,
        name="_query_scorecard_api",
        value=_mock_query_scorecard_api(ScorecardResponse(**scorecard_response)),
    )

    result = plugin.process_component(component)
    assert result.is_success()
    assert result.return_obj == expected_component
