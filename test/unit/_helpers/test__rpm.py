"""
Test module for the RPMScorecardHelper class
"""

from __future__ import annotations

import gzip

from collections import OrderedDict
from typing import Callable

import httpx
import pytest

from hoppr import CredentialRequiredService, Credentials
from pytest import MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._rpm import RPMScorecardHelper


TEST_REPOMD_XML = """<?xml version="1.0" encoding="UTF-8"?>
<repomd xmlns="http://rpm.hoppr.com/metadata/repo" xmlns:rpm="http://rpm.hoppr.com/metadata/rpm">
  <revision>1667635478</revision>
  <data type="primary">
    <location href="repodata/test-primary.xml.gz"/>
  </data>
</repomd>"""

TEST_PRIMARY_XML_GZ = b"0" * 1024

TEST_PRIMARY_XML = """<?xml version="1.0" encoding="UTF-8"?>
<metadata xmlns="http://rpm.hoppr.com/metadata/common" xmlns:rpm="http://rpm.hoppr.com/metadata/rpm" packages="2">
<package type="rpm">
  <name>hoppr-test-rpm-1</name>
  <arch>x86_64</arch>
  <version epoch="0" ver="1.2.3" rel="1.fc37"/>
  <url>https://github.com/hoppr/test-rpm-1</url>
</package>
<package type="rpm">
  <name>hoppr-test-rpm-2</name>
  <arch>x86_64</arch>
  <version epoch="0" ver="1.2.3" rel="1.fc37"/>
  <url>https://github.com/hoppr/test-rpm-2</url>
</package>
</metadata>"""

TEST_RPM_DATA = {
    "metadata": {
        "@xmlns": "http://rpm.hoppr.com/metadata/common",
        "@xmlns:rpm": "http://rpm.hoppr.com/metadata/rpm",
        "@packages": "2",
        "package": [
            {
                "@type": "rpm",
                "name": "hoppr-test-rpm-1",
                "arch": "x86_64",
                "version": {"@epoch": "0", "@ver": "1.2.3", "@rel": "1.fc37"},
                "url": "https://github.com/hoppr/test-rpm-1",
            },
            {
                "@type": "rpm",
                "name": "hoppr-test-rpm-2",
                "arch": "x86_64",
                "version": {"@epoch": "0", "@ver": "1.2.3", "@rel": "1.fc37"},
                "url": "https://github.com/hoppr/test-rpm-2",
            },
        ],
    }
}


@pytest.fixture(name="helper", scope="function", params=({"context": "context"},))
def helper_fixture(helper: RPMScorecardHelper) -> RPMScorecardHelper:
    """
    Fixture to return RPMScorecardHelper instance
    """
    return helper


async def test__populate_rpm_data(helper: RPMScorecardHelper, httpx_mock: HTTPXMock, monkeypatch: MonkeyPatch):
    """
    Test RPMScorecardHelper._populate_rpm_data method
    """
    repo = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"

    httpx_mock.add_response(url=f"{repo}/repodata/repomd.xml", status_code=200, text="<fail-data/>")
    await helper._populate_rpm_data(repo)  # pylint: disable=protected-access
    assert helper.rpm_data.get(repo) is None

    monkeypatch.setattr(target=gzip, name="decompress", value=lambda data: TEST_PRIMARY_XML)

    httpx_mock.add_response(url=f"{repo}/repodata/repomd.xml", status_code=200, text=TEST_REPOMD_XML)
    httpx_mock.add_response(url=f"{repo}/repodata/test-primary.xml.gz", status_code=200, content=TEST_PRIMARY_XML_GZ)

    await helper._populate_rpm_data(  # pylint: disable=protected-access
        repo, httpx.BasicAuth(username="test_user", password="test_password")
    )

    assert helper.rpm_data[repo] == TEST_RPM_DATA


async def test__repo_generator_missing_distro(helper: RPMScorecardHelper):
    """
    Test RPMScorecardHelper._repo_generator method raises ValueError if missing Fedora distro in purl string
    """
    assert helper.context
    helper.context.repositories = type(helper.context.repositories).parse_obj({"rpm": []})

    with pytest.raises(
        expected_exception=ValueError,
        match="PURL version '1.2.3-1.fc' must contain Fedora release version.",
    ):
        async for _ in helper._repo_generator(arch="x86_64", version="1.2.3-1.fc"):  # pylint: disable=protected-access
            pass


@pytest.mark.parametrize(
    argnames=["rpm_repos", "purl_string"],
    argvalues=[
        ([{"url": "https://rpm.hoppr.com/os"}], "pkg:rpm/hoppr-test-rpm-1@1.2.3-1.fc37?arch=x86_64"),
        ([], "pkg:rpm/hoppr-test-rpm-1@1.2.3-1.el7?arch=x86_64"),
        ([], "pkg:rpm/hoppr-test-rpm-1@1.2.3-1.el8?arch=x86_64"),
        ([], "pkg:rpm/hoppr-test-rpm-1@1.2.3-1.el9?arch=x86_64"),
        ([], "pkg:rpm/hoppr-test-rpm-1@1.2.3-1.fc37?arch=x86_64"),
    ],
)
async def test_get_vcs_repo_url(
    helper: RPMScorecardHelper,
    rpm_repos: list[dict[str, str]],
    purl_string: str,
    find_credentials: Callable[[str], CredentialRequiredService],
    monkeypatch: MonkeyPatch,
):
    """
    Test RPMScorecardHelper.get_vcs_repo_url method
    """

    async def _mock_populate_rpm_data(
        repo: str, auth: httpx.BasicAuth | None = None  # pylint: disable=unused-argument
    ) -> None:
        helper.rpm_data[repo] = OrderedDict(TEST_RPM_DATA)

    monkeypatch.setattr(target=helper, name="_populate_rpm_data", value=_mock_populate_rpm_data)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials)

    assert helper.context
    helper.context.repositories = type(helper.context.repositories).parse_obj({"rpm": rpm_repos})

    result = await helper.get_vcs_repo_url(purl_string)
    assert result == ["https://github.com/hoppr/test-rpm-1"]
