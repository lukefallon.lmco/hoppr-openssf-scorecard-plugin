"""
Test module for the RubyGemsScorecardHelper class
"""

from __future__ import annotations

import re

from typing import Any

import httpx
import pytest

from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._gem import RubyGemsScorecardHelper


@pytest.mark.parametrize(
    argnames=["response", "expected_result"],
    argvalues=[
        (
            {"source_code_uri": "https://github.com/hoppr/test-repo/tree/v1.2.3"},
            ["https://github.com/hoppr/test-repo"],
        ),
        (
            {"homepage_uri": "https://github.com/hoppr/test-repo"},
            ["https://github.com/hoppr/test-repo"],
        ),
        (
            {"bug_tracker_uri": "https://github.com/hoppr/test-repo/issues"},
            ["https://github.com/hoppr/test-repo"],
        ),
        (
            {"changelog_uri": "https://github.com/hoppr/test-repo/blob/master/bundler/CHANGELOG.md"},
            ["https://github.com/hoppr/test-repo"],
        ),
        (
            {"wiki_uri": "https://github.com/hoppr/test-repo/wiki"},
            ["https://github.com/hoppr/test-repo"],
        ),
        ({}, []),
    ],
)
async def test_get_vcs_repo_url(
    helper: RubyGemsScorecardHelper, response: dict[str, Any], expected_result: list[str], httpx_mock: HTTPXMock
):
    """
    Test RubyGemsScorecardHelper.get_vcs_repo_url method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/gems/.*\\.json$"),
        status_code=200,
        json=response,
    )

    result = await helper.get_vcs_repo_url(purl_string="pkg:gem/hoppr/test-gem@1.2.3")
    assert result == expected_result


async def test_get_vcs_repo_url_not_found_error(helper: RubyGemsScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test RubyGemsScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/gems/.*\\.json$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url(purl_string="pkg:gem/hoppr/test-gem@1.2.3")
        assert result is None


async def test_get_vcs_repo_url_timeout(helper: RubyGemsScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test RubyGemsScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/gems/.*\\.json$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url(purl_string="pkg:gem/hoppr/test-gem@1.2.3")
        assert result is None
