"""
Test module for the GitScorecardHelper class
"""

from __future__ import annotations

import logging
import re
import sys

from typing import Literal

import httpx
import pytest

from pytest import FixtureRequest, LogCaptureFixture, MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._git import GitScorecardHelper
from hoppr_openssf_scorecard._helpers._github import GitHubScorecardHelper
from hoppr_openssf_scorecard._helpers._gitlab import GitLabScorecardHelper


github_200 = {
    "url": re.compile(pattern=f"^{GitHubScorecardHelper.API_URL}/search/repositories.*$"),
    "status_code": 200,
    "json": {"items": [{"name": "test-project", "html_url": "https://github.com/hoppr/test-project"}]},
}

github_404 = {
    "url": re.compile(pattern=f"^{GitHubScorecardHelper.API_URL}/search/repositories.*$"),
    "status_code": 404,
}

gitlab_200 = {
    "url": re.compile(pattern=f"^{GitLabScorecardHelper.API_URL}/projects/.*$"),
    "status_code": 200,
    "json": {"web_url": "https://gitlab.com/hoppr/test-project"},
}

gitlab_404 = {
    "url": re.compile(pattern=f"^{GitLabScorecardHelper.API_URL}/projects/.*$"),
    "status_code": 404,
}


@pytest.fixture(scope="function", autouse=True)
def setup_fixture(monkeypatch: MonkeyPatch):
    """
    Pre-test fixture to automatically patch sleep methods
    """
    monkeypatch.setattr(target="hoppr_openssf_scorecard._helpers._git.sleep", name=lambda secs: None)
    monkeypatch.setattr(target=GitHubScorecardHelper, name="await_rate_limit_reset", value=_mock_await_rate_limit_reset)


@pytest.fixture(name="httpx_mock_response", scope="function")
def httpx_mock_response_fixture(request: FixtureRequest, httpx_mock: HTTPXMock) -> None:
    """
    Fixture to set up parametrized httpx_mock responses
    """
    response_list: list[Literal["github_200", "github_404", "gitlab_200", "gitlab_404"]] = getattr(request, "param", [])

    for response in response_list:
        response_dict = getattr(sys.modules[__name__], response)
        httpx_mock.add_response(**response_dict)


async def _mock_await_rate_limit_reset():
    return None


@pytest.mark.usefixtures("httpx_mock_response")
@pytest.mark.parametrize(
    argnames=["manifest_repos", "httpx_mock_response", "expected_result"],
    argvalues=[
        ([{"url": "https://github.com"}], ["github_200"], ["https://github.com/hoppr/test-project"]),
        ([{"url": "https://gitlab.com"}], ["gitlab_200"], ["https://gitlab.com/hoppr/test-project"]),
        ([], ["github_200"], ["https://github.com/hoppr/test-project"]),
        ([], ["github_404", "gitlab_200"], ["https://gitlab.com/hoppr/test-project"]),
        ([], ["github_404", "gitlab_404"], []),
    ],
    indirect=["httpx_mock_response"],
)
async def test_get_vcs_repo_url(
    helper: GitScorecardHelper, manifest_repos: list[dict[str, str]], expected_result: list[str]
):
    """
    Test GitScorecardHelper.get_vcs_repo_url method
    """
    assert helper.context
    helper.context.repositories = type(helper.context.repositories).parse_obj({"git": manifest_repos})

    result = await helper.get_vcs_repo_url("pkg:git/hoppr/test-project.git")
    assert result == expected_result


async def test_get_vcs_repo_url_unsupported_manifest_repo(helper: GitScorecardHelper):
    """
    Test GitScorecardHelper.get_vcs_repo_url method with unsupported manifest repository
    """
    assert helper.context
    helper.context.repositories = type(helper.context.repositories).parse_obj({"git": [{"url": "https://gitbox.com"}]})

    result = await helper.get_vcs_repo_url("pkg:git/hoppr/test-project.git")
    assert result == []


async def test_get_vcs_repo_url_not_found_error(
    helper: GitScorecardHelper,
    caplog: LogCaptureFixture,
    monkeypatch: MonkeyPatch,
    httpx_mock: HTTPXMock,
):
    """
    Test GitScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    assert helper.context
    helper.context.repositories = type(helper.context.repositories).parse_obj({"git": [{"url": "https://github.com"}]})

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{GitHubScorecardHelper.API_URL}/search/repositories.*$"),
        status_code=404,
    )

    monkeypatch.setattr(target=helper, name="_logger", value=logging.getLogger("HopprPytest"))

    with caplog.at_level(level=logging.INFO, logger="HopprPytest"):
        result = await helper.get_vcs_repo_url("pkg:git/hoppr/test-project.git")

    assert result == []
    assert caplog.messages[0] == "Requesting component VCS URL from GitHub API"
    assert caplog.messages[1] == "Unable to find GitHub repository"


async def test_get_vcs_repo_url_timeout(helper: GitScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test GitScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    assert helper.context
    helper.context.repositories = type(helper.context.repositories).parse_obj({"git": [{"url": "https://github.com"}]})

    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{GitHubScorecardHelper.API_URL}/search/repositories.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url("pkg:git/hoppr/test-project.git")
        assert result is None
