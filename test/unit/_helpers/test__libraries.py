"""
Test module for the LibrariesIOScorecardHelper class
"""

from __future__ import annotations

import logging
import re

from typing import Any, Callable

import httpx
import pytest

from hoppr import CredentialRequiredService, Credentials
from pytest import LogCaptureFixture, MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._libraries import LibrariesIOScorecardHelper


@pytest.fixture(scope="function", autouse=True)
def setup_fixture(monkeypatch: MonkeyPatch):
    """
    Pre-test fixture to automatically patch sleep methods
    """
    monkeypatch.setenv(name="LIBRARIES_API_KEY", value="TEST_LIBRARIES_API_KEY")
    monkeypatch.setattr(target="hoppr_openssf_scorecard._helpers._libraries.sleep", name=lambda secs: None)


@pytest.fixture(name="httpx_mock_response", scope="function")
def httpx_mock_response_fixture(libraries_io_response: list[dict[str, Any]], httpx_mock: HTTPXMock) -> None:
    """
    Fixture to return mocked response from Libraries.io
    """
    for idx, response_item in enumerate(libraries_io_response, start=1):
        httpx_mock.add_response(
            url=re.compile(pattern=f"^{LibrariesIOScorecardHelper.API_URL}/search.*page={idx}.*$"),
            status_code=200,
            json=[response_item],
        )

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{LibrariesIOScorecardHelper.API_URL}/search.*$"), status_code=200, json=[]
    )


@pytest.mark.usefixtures("httpx_mock_response", "assert_all_responses_were_requested")
@pytest.mark.parametrize(
    argnames=["libraries_io_response", "purl_string", "expected_result", "assert_all_responses_were_requested"],
    argvalues=[
        (["no-match", "test-pypi"], "pkg:pypi/test-pypi@1.2.3", ["https://github.com/hoppr/test-pypi"], True),
        (["no-match", "test-mvn"], "pkg:maven/test-mvn@1.2.3", ["https://github.com/hoppr/test-mvn"], True),
        (["no-match", "test-go"], "pkg:golang/test-go@1.2.3", ["https://github.com/hoppr/test-go"], True),
        (["no-match", "test-npm"], "pkg:npm/test-npm@1.2.3", ["https://github.com/hoppr/test-npm"], True),
        (["no-match", "test-npm"], "pkg:npm/test-npm@1.2.3", ["https://github.com/hoppr/test-npm"], True),
        (["no-match", "test-rpm"], "pkg:rpm/test-rpm@1.2.3", ["https://github.com/hoppr/test-rpm"], True),
        (["test-rpm", "test-rpm"], "pkg:rpm/test-rpm@1.2.3", [], False),
        (["no-match-1", "no-match-2"], "pkg:pypi/test-pypi@1.2.3", [], True),
        ([], "pkg:pypi/test-pypi@1.2.3", [], True),
    ],
    indirect=["libraries_io_response"],
)
async def test_get_vcs_repo_url(helper: LibrariesIOScorecardHelper, purl_string: str, expected_result: list[str]):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method
    """
    result = await helper.get_vcs_repo_url(purl_string)
    assert result == expected_result


@pytest.mark.usefixtures("httpx_mock_response", "assert_all_responses_were_requested")
@pytest.mark.parametrize(
    argnames=["libraries_io_response", "assert_all_responses_were_requested"],
    argvalues=[
        (
            ["test-1", "test-2", "test-3", "test-4", "test-5", "test-6", "test-7", "test-8", "test-9", "test-10"],
            False,
        )
    ],
    indirect=["libraries_io_response"],
)
async def test_get_vcs_repo_url_bad_relevance_score(
    helper: LibrariesIOScorecardHelper,
    caplog: LogCaptureFixture,
    monkeypatch: MonkeyPatch,
):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method with score exceeding relevance threshold
    """
    monkeypatch.setattr(target=helper, name="_logger", value=logging.getLogger("HopprPytest"))
    monkeypatch.setattr(
        target=helper._logger,  # pylint: disable=protected-access
        name="info",
        value=lambda msg, *_, **__: logging.info(msg),
    )

    # Without an exact match, score should be calculated to exceed relevance threshold
    with caplog.at_level(level=logging.INFO, logger="HopprPytest"):
        result = await helper.get_vcs_repo_url("pkg:pypi/test@1.2.3")

    assert result == []
    assert caplog.messages[-1] == "Best match did not meet relevance requirement"


async def test_get_vcs_repo_url_no_api_key(helper: LibrariesIOScorecardHelper, monkeypatch: MonkeyPatch):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method raises EnvironmentError
    """
    monkeypatch.delenv(name="LIBRARIES_API_KEY", raising=False)

    with pytest.raises(
        expected_exception=EnvironmentError,
        match=(
            "Either a credentials file with an entry for 'https://libraries.io/api' "
            "or the environment variable LIBRARIES_API_KEY must be set to use this plugin."
        ),
    ):
        await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")


async def test_get_vcs_repo_url_not_found_error(helper: LibrariesIOScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/search.*$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None


async def test_get_vcs_repo_url_timeout(helper: LibrariesIOScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/search.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None


@pytest.mark.usefixtures("httpx_mock_response", "assert_all_responses_were_requested")
@pytest.mark.parametrize(
    argnames=["cred_object", "assert_all_responses_were_requested"],
    argvalues=[({"url": LibrariesIOScorecardHelper.API_URL}, False)],
    indirect=["cred_object"],
)
async def test_get_vcs_repo_url_with_credentials(
    helper: LibrariesIOScorecardHelper,
    find_credentials: Callable[[str], CredentialRequiredService],
    monkeypatch: MonkeyPatch,
):
    """
    Test LibrariesIOScorecardHelper.get_vcs_repo_url method with parsed Credentials input
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials)

    result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
    assert result == ["https://github.com/hoppr/test-pypi-pkg"]
