## [0.0.2-dev.3](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.2-dev.2...v0.0.2-dev.3) (2024-02-08)


### Bug Fixes

* add retries to HTTP client ([427543f](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/427543f122585f50107f18e7255a366933c3cc96))
* remove HTTP client retries, update deb test data ([81396a9](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/81396a935859410b15e3e6269a44ffc3303356ec))

## [0.0.2-dev.2](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.2-dev.1...v0.0.2-dev.2) (2023-12-14)


### Bug Fixes

* 23.10 could not install python3.10 so using 22.04 ([9dfeadd](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/9dfeadda81102cbde253aa04de5c6cc570b59520))
* attempting a different ubuntu tag ([03143ec](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/03143ec382db522b92786cc6e56b91bb7f71393a))
* correcting formatting ([c4c1504](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/c4c1504d51e868148393f0fcd0df3bede61150cb))
* updating Dockerfile to hoppr std and pylint complaint ([e2ef0d7](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/e2ef0d7c67216c9e4f829f7413c33d837003317a))
* updating maven helper to use Central directly rather than search ([f31c3ab](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/f31c3ab23191f9db91c9dbe4ccd4ee0d05ae8bf3))
* updating the ci docker image ([bff0216](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/bff0216f2936e98a054e45962147dfc4b00d9ef7))
* updating timeout for libraries.io and fixing mypy findings ([da4bcb6](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/da4bcb6a98d4b471c5adfd4ce84ba4f7f20abd3b))

## [0.0.2-dev.1](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.1...v0.0.2-dev.1) (2023-10-03)


### Bug Fixes

* BOM access and lint errors ([39b7236](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/39b72366e349cea8e7e2aa0586c9d88de4d139d3))
* update logger import, fix unit tests ([e9a4365](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/e9a4365ce6176c975653163e95109bc51dcc9209))

## [0.0.1](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.0...v0.0.1) (2023-04-05)


### Bug Fixes

* improve Libraries.io best match logic ([eaf1d7b](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/eaf1d7bb1ff962a3bc41b82cab3d6c7c5d1660a9))
* improved scorecard component properties ([4bb2a24](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/4bb2a247d1a241d49986c3d232e5ec7f8bd417ed))
* Libraries.io response model types ([41ca418](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/41ca4189ab01088d4cea31583dbd1d52ba501e51))
* plugin SBOM access ([32b6038](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/32b6038ef011a36bd3d837f06a17e6c542b8d53a))
* remove refs to `trio_typing` test dependency ([bee99e4](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/bee99e45bacc26b38583f4cf211f53222aff829d))

## [0.0.1-dev.2](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.1-dev.1...v0.0.1-dev.2) (2023-04-03)


### Bug Fixes

* remove refs to `trio_typing` test dependency ([bee99e4](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/bee99e45bacc26b38583f4cf211f53222aff829d))

## [0.0.1-dev.1](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/compare/v0.0.0...v0.0.1-dev.1) (2023-03-24)


### Bug Fixes

* improve Libraries.io best match logic ([eaf1d7b](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/eaf1d7bb1ff962a3bc41b82cab3d6c7c5d1660a9))
* improved scorecard component properties ([4bb2a24](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/4bb2a247d1a241d49986c3d232e5ec7f8bd417ed))
* Libraries.io response model types ([41ca418](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/41ca4189ab01088d4cea31583dbd1d52ba501e51))
* plugin SBOM access ([32b6038](https://gitlab.com/hoppr/plugins/hoppr-openssf-scorecard-plugin/commit/32b6038ef011a36bd3d837f06a17e6c542b8d53a))

<!--next-version-placeholder-->
